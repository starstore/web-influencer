
const type = document.location.protocol;
const baseURL = type + '//192.168.31.20';
// const baseURL = type + '//api.star.store';

export default {
    baseURL: baseURL,
    logURL: baseURL + '/uploads/',
    uploadURL: baseURL + '/upload',
    defaultImg: 'logo.png'
}
