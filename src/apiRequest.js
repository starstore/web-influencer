import axios from "axios";
import qs from 'qs';
import obj from './config.js'

function baseRequest(method, path, params, data, type) {
    method = method.toUpperCase() || 'GET';
    axios.defaults.baseURL = obj.baseURL;
    axios.defaults.headers['access-token'] = sessionStorage.getItem('dataToken');
    if (method === 'POST') {
        axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        let o=axios.post(path, qs.stringify(data));
        o.then((data)=>{
          if(data.data.code==99){
            // 跳转到登录界面
            window.location.href='/#/login';
          }else{
          }
        })
        return o
    } else if (method === 'GET') {
        let o=axios.get(path, { params: params });
        o.then((data)=>{
          if(data.data.code==99){
            // 跳转到登录界面
            window.location.href='/#/login';
          }else{
          }
        })
        return o
    } else {
        return axios.delete(path, qs.stringify(data));
    };
}

export let baseul = obj.baseURL;

export let addNewUser = function(data) {
    return baseRequest('POST', '/influencer/reg', '', data);
};

export let getVerification = function(params) {
    return baseRequest('GET', '/brand/auth', params, '');
};

export let login = function(data) {
    return baseRequest('POST', '/influencer/login', '', data);
};

export let auth = function(params) {
    return baseRequest('GET', '/influencer/auth', params, '');
};

export let getCampaigns = function(params) {
    return baseRequest('GET', '/influencer/campaigns', params, '');
};

export let setPages = function(data) {
    return baseRequest('POST', '/influencer/page_setting', '', data);
};

export let checkPages = function(params) {
    return baseRequest('GET', '/influencer/influ_page_exist', params, '');
};

export let changePassword = function(data) {
    return baseRequest('POST', '/influencer/pwd_change', '', data);
};

export let getTakeCampaign = function(params) {
    return baseRequest('GET', '/influencer/influ_campaign_taked_detail', params, '');
};

export let getMyTakeCampaign = function(params) {
    return baseRequest('GET', '/api/1.0/influencer/campaign/mycpdetail', params, '');
};

export let conectProductAndInfluencer = function(data) {
    return baseRequest('POST', '/influencer/influ_campaign_create', '', data);
};

export let getMyInfluencerList = function(params) {
    return baseRequest('GET', '/influencer/influ_taked_campaings', params, '');
};

export let createInfluencerPost = function(data) {
    return baseRequest('POST', '/influencer/influ_feed_create', '', data);
};

export let setInfluencerSocial = function(data) {
    return baseRequest('POST', '/influencer/set_influ_user_socialplatform', '', data);
};

export let getInfluencerSocial = function(params) {
    return baseRequest('GET', '/influencer/get_influ_user_socialplatform', params, '');
};

export let resetPassword = function(params) {
    return baseRequest('GET', '/services/sendmail', params, '');
};

// export let resetConmitPassword = function(params) {
//     return baseRequest('GET', '/services/get_resetpwd', params, '');
// };

export let resetNewPassword = function(data) {
    return baseRequest('POST', '/services/resetpwd', '', data);
};
// get msg data
// ck 去重
export let getCkHourly = function(params) {
    return baseRequest('GET', '/ck_hourly_1', params, '');
};
// pb 去重
export let getPbHourly = function(params) {
    return baseRequest('GET', '/pb_hourly_1', params, '');
};
export let getPackageMsg = function(params) {
    return baseRequest('GET', '/influencer/influ_campaign_pacakge', params, '');
};

export let chaeckInfluencerUser = function(params) {
    return baseRequest('GET', '/influencer/influ_email_exist', params, '');
};

export let getacssIdByInfluId = function(params) {
    return baseRequest('GET', '/influencer/get_influ_campaign_id', params, '');
};

export let getInfluWallet = function(params) {
    return baseRequest('GET', '/influencer/get_influ_wallet', params, '');
};

export let setInfluWallet = function(data) {
    return baseRequest('POST', '/influencer/influ_wallet_get', '', data);
};

export let takeMoreCamopaign = function(data) {
    return baseRequest('POST', '/influencer/influ_take_campaign_uri', '', data);
};

export let getInfluWalletList = function(params) {
    return baseRequest('GET', '/influencer/get_influ_wallet_history', params, '');
};

export let getAllCampaignList = function(params) {
    return baseRequest('GET', '/influencer/allcampaigns', params, '');
};

export let checkCurrentStates = function(params) {
    return baseRequest('GET', '/influencer/influ_taked_campaign_count', params, '');
};

export let check_campaign_state = function(params) {
    return baseRequest('GET', '/influencer/check_campaign_state', params, '');
};

export let getAbnameExist = function(params) {
    return baseRequest('GET', '/operation/check_campaign_abname', params, '');
};

export let getCampaignsCountryList = function(params) {
    return baseRequest('GET', '/influencer/influ_get_country', params, '');
};

export let checkShortName = function(params) {
    return baseRequest('GET', '/influencer/check_campaign_uri', params, '');
};

export let checkVerification = function(params) {
    return baseRequest('GET', '/influencer/checkVerification', params, '');
};
export let verification = function(params) {
    return baseRequest('GET', '/influencer/verification', params, '');
};
export let updateInfluencerDetail = function updateinflu(data) {
    return baseRequest('POST', '/influencer/influ_profile_setting', '', data);
};
export let getInfluById = function(params) {
    return baseRequest('GET', '/influencer/get_influ_by_id', params, '');
};
export let influ_wallet_get_exist = function(params) {
    return baseRequest('GET', '/influencer/influ_wallet_get_exist', params, '');
};
export let influ_taked_campaings = function(params) {
  return baseRequest('GET', '/influencer/influ_taked_campaings', params, '');
};
export let setInflupwd = function updateinflu(params) {
  return baseRequest('POST', '/influencer/setinflupwd', '', params);
};
export let socialmedia = function(params) {
  return baseRequest('GET', '/api/1.0/influencer/socialmedia/slist', params, '');
};
export let camlist = function(params) {
  return baseRequest('GET', '/api/1.0/influencer/campaign/cplist', params, '');
};
export let takecp = function updateinflu(params) {
  return baseRequest('POST', '/api/1.0/influencer/campaign/takecp', '', params);
};

export let getReport = function(params) {
  return baseRequest('GET', '/api/1.0/influencer/campaign/getreport', params, '');
};

export let cpdetail = function(params) {
  return baseRequest('GET', '/api/1.0/influencer/campaign/cpdetail', params, '');
};

export let cppostlink = function(params) {
  return baseRequest('GET', '/api/1.0/influencer/socialmedia/cppostlink', params, '');
};

export let recpagename = function(params) {
  return baseRequest('GET', '/api/1.0/influencer/profile/recpagename', params, '');
};
