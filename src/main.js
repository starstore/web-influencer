// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-default/index.css';
import 'font-awesome/css/font-awesome.min.css';
import './index.css';
import { auth } from './apiRequest';
import Promise from 'whatwg-fetch';
import 'babel-polyfill';
import locale from 'element-ui/lib/locale/lang/en';
import vuescroll from 'vue-scroll';
// import VueI18n from 'vue-i18n';
// import enLocale from 'element-ui/lib/locale/lang/en';
// import zhLocale from 'element-ui/lib/locale/lang/zh-CN';

Vue.config.productionTip = false;
Vue.use(ElementUI, { locale });
Vue.use(vuescroll);
// Vue.use(VueI18n);
// Vue.config.lang = 'zh-cn';
// Vue.locale('zh-cn', zhLocale);
// Vue.locale('en', enLocale);
if (!window.Promise) {
    window.Promise = Promise;
}

router.beforeEach((to, from, next) => {
    if (to.path === '/login' || to.path === '/addNewUser' || to.path === '/forgetPassword' || to.path === '/resetPassWord' || to.path === '/verification' || to.path === '/auth' || to.path === '/authsign' || to.path === '/enterinf') {
        return next();
    }
    if(sessionStorage.getItem('dataToken')){
      // console.log('开始检测 token');
      auth({ token: sessionStorage.getItem('dataToken') }).then((data) => {
        if (data.data.code === 0) {
          // console.log('验证通过', data.data);
          next();
        } else {
          // console.log('验证不通过，跳登陆页面');
          next({ path: '/login' });
        }
      }).catch((res) => {
        // console.log(res);
        next({ path: '/login' });
      });
    }
    else{
        next({ path: '/login' });
    }
});

new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
})
