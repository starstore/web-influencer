import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';

import AddNewUser from '@/components/user/AddNewUser';
import Login from '@/components/user/Login';
import ForgetPassword from '@/components/user/ForgetPassword';
import ResetPassWord from '@/components/user/ResetPassWord';
import Verification from '@/components/user/Verification';
import AuthLogin from '@/components/user/AuthLogin';
import AuthSign from '@/components/user/AuthSign';
import EnterInflu from '@/components/user/EnterInflu';
// import CampaignList_ca from '../components/page/CampaignList_ca.vue';
import CampaignList from '@/components/page/CampaignList'
import MyCampaign from '@/components/page/MyCampaign';
import MyCampaignDetails from '@/components/page/MyCampaignDetails';
import Setting from '@/components/page/Setting';
import Wallet from '@/components/page/Wallet';
import SuppportCenter from '@/components/page/Faq';
import Promotion from '@/components/page/Promotion';
import Take from '@/components/page/Take';
import CampaignDetails from '@/components/page/CampaignDetails';
import HistoryReport from '@/components/page/HistoryReport';
import Qsearch from '@/components/page/Qsearch'
Vue.use(Router);

export default new Router({
    routes: [{
            path: '/home',
            component: Home,
            children: [
                { path: 'campaignList/:param', name: 'campaignList', component: CampaignList },
                { path: 'myCampaign', name: 'myCampaign', component: MyCampaign },
                { path: 'take/:id', name: 'take', component: Take },
                { path: 'myCampaignDetails/:id/:state', name: 'MyCampaignDetails', component: MyCampaignDetails },
                { path: 'campaignDetails/:id/:state', name: 'campaignDetails', component: CampaignDetails },
                { path: 'wallet/:param', name: 'wallet', component: Wallet },
                { path: 'setting/:param?', name: 'setting', component: Setting },
                { path: 'supportCenter/:param', name: 'faq', component: SuppportCenter },
                { path: 'promotion', name: 'promotion', component: Promotion },
                { path: 'hreport',name:'hreport',component:HistoryReport},
                { path: 'qsearch',name:'qsearch',component:Qsearch},
            ]
        },
        { path:'/enterinf',name:'enterInflu',component:EnterInflu},
        { path:'/authsign',name:'authSign',component:AuthSign},
        { path:'/auth',name:'authLogin',component:AuthLogin },
        { path: '/addNewUser', name: 'addNewUser', component: AddNewUser },
        { path: '/forgetPassword', name: 'forgetPassword', component: ForgetPassword },
        { path: '/resetPassWord', name: 'resetPassWord', component: ResetPassWord },
        { path: '/verification', name: 'verification', component: Verification },
        { path: '/login', name: 'login', component: Login },
        { path: '/', redirect: '/home' }
    ]
})
